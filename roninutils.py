from web3 import Web3

RONIN_PROVIDER_FREE = "https://proxy.roninchain.com/free-gas-rpc"
RONIN_RPC = "https://proxy.roninchain.com/free-gas-rpc"
AXIE_CONTRACT = "0x32950db2a7164ae833121501c797d79e7b79d74c"
WETH_CONTRACT = "0xc99a6a985ed2cac1ef41640596c5a5f9f4e19ef5"
AXS_CONTRACT = "0x97a9107c1793bc407d6f527b77e7fff4d812bece"
SLP_CONTRACT = "0xa8754b9fa15fc18bb59458815510e40a12cd2014"
MKT_CONTRACT = "0x213073989821f738a7ba3520c3d31a1f9ad31bbd"
SCATTER_CONTRACT = "0x14978681c5f8ce2f6b66d1f1551b0ec67405574c"

USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1944.0 Safari/537.36" # noqa

def getWeb3Provider():
    return Web3(
            Web3.HTTPProvider(
                RONIN_PROVIDER_FREE,
                request_kwargs={"headers": {"content-type": "application/json", "user-agent": USER_AGENT}}))

def signTransaction(transaction, pkey):    
    signed = Web3().eth.account.sign_transaction(
            transaction,
            private_key=pkey
        )    
    print(f'data:{Web3.toHex(signed.rawTransaction)}')
    