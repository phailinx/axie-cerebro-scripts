from web3 import Web3
import os
from encrypt import getPKey
script_dir = os.path.dirname(os.path.dirname( __file__ ))
import roninutils

abiPath = rf"{script_dir}/abi/slp.json"
contractAdd = roninutils.SLP_CONTRACT

class RonToken:
    def __init__(self, owner):
        self.owner = owner
        self.pkey = getPKey(owner).decode('ascii')
    
    def transfer(self, toAcc, amount, gas, gasPrice, nonce):
        transaction = {
            'to': Web3.toChecksumAddress(toAcc),
            'value': int(amount),
            'gas': int(gas),
            'gasPrice': int(gasPrice),
            'nonce': int(nonce),
            'chainId': 2020
        }

        roninutils.signTransaction(transaction, self.pkey)