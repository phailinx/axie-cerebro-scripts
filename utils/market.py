import os
import json
from web3 import Web3
from encrypt import getPKey
script_dir = os.path.dirname(os.path.dirname( __file__ ))
import roninutils

abiPath = rf"{script_dir}/abi/mkt.json"
contractAdd = roninutils.MKT_CONTRACT

class MktContract:
    def __init__(self, owner):
        self.w3 = roninutils.getWeb3Provider()
        self.owner = owner
        self.pkey = getPKey(owner).decode('ascii')
        with open(abiPath, encoding='utf-8') as f:
            abi = json.load(f)
        self.contract = self.w3.eth.contract(
            address=Web3.toChecksumAddress(contractAdd),
            abi=abi
        )
    
    def cancelAuction(self, listingIndex, gas, gasPrice, nonce):
        transaction = self.contract.functions.cancelAuction(
            int(listingIndex)            
        ).buildTransaction({ 'gas': int(gas), 'gasPrice': int(gasPrice), 'nonce': int(nonce) })
        roninutils.signTransaction(transaction, self.pkey)        

    def createAuction(self, axieId, startingPrice, endPrice, duration, gas, gasPrice, nonce):
        transaction = self.contract.functions.createAuction(
            [1],
            [Web3.toChecksumAddress(roninutils.AXIE_CONTRACT)],
            [int(axieId)],
            [int(startingPrice)],
            [int(endPrice)],
            [Web3.toChecksumAddress(roninutils.WETH_CONTRACT)],
            [int(duration)]
        ).buildTransaction({ 'gas': int(gas), 'gasPrice': int(gasPrice), 'nonce': int(nonce) })
        roninutils.signTransaction(transaction, self.pkey)        