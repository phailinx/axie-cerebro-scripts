import os
import json
from web3 import Web3
from encrypt import getPKey
script_dir = os.path.dirname(os.path.dirname( __file__ ))
import roninutils

abiPath = rf"{script_dir}/abi/scatter.json"
contractAdd = roninutils.SCATTER_CONTRACT

class ScatterContract:
    def __init__(self, owner):
        self.w3 = roninutils.getWeb3Provider()
        self.owner = owner
        self.pkey = getPKey(owner).decode('ascii')
        with open(abiPath, encoding='utf-8') as f:
            self.abi = json.load(f)
        self.contract = self.w3.eth.contract(
            address=Web3.toChecksumAddress(contractAdd),
            abi=self.abi
        )        
    
    def approveToken(self, token, gas, gasPrice, nonce):
        tokenContract = self.w3.eth.contract(
            address=Web3.toChecksumAddress(token),
            abi=self.abi
        )
        transaction = tokenContract.functions.approve(
            Web3.toChecksumAddress(contractAdd),
            pow(2, 256) - 1
        ).buildTransaction({ 'gas': int(gas), 'gasPrice': int(gasPrice), 'nonce': int(nonce) })
        roninutils.signTransaction(transaction, self.pkey)

    def scatterTokens(self, token, recipients, values, gas, gasPrice, nonce): 
        recipients = list(map(lambda x: Web3.toChecksumAddress(x), recipients.split(",")))
        values = list(map(lambda x:int(x), values.split(",")))

        totalRon = 0
        for val in values:
            totalRon = totalRon + val

        if token == "ron":
            transaction = self.contract.functions.disperseEther(
                recipients,
                values
            ).buildTransaction({ 'value': totalRon, 'gas': int(gas), 'gasPrice': int(gasPrice), 'nonce': int(nonce) })
        else:    
            transaction = self.contract.functions.disperseToken(
                Web3.toChecksumAddress(token),
                recipients,
                values
            ).buildTransaction({ 'gas': int(gas), 'gasPrice': int(gasPrice), 'nonce': int(nonce) })
        roninutils.signTransaction(transaction, self.pkey)            