import os
import json
from web3 import Web3
from encrypt import getPKey
script_dir = os.path.dirname(os.path.dirname( __file__ ))
import roninutils

abiPath = rf"{script_dir}/abi/axie.json"
contractAdd = roninutils.AXIE_CONTRACT

class AxieContract:
    def __init__(self, owner):
        self.w3 = roninutils.getWeb3Provider()
        self.owner = owner
        self.pkey = getPKey(owner).decode('ascii')
        with open(abiPath, encoding='utf-8') as f:
            abi = json.load(f)
        self.contract = self.w3.eth.contract(
            address=Web3.toChecksumAddress(contractAdd),
            abi=abi
        )
    
    def transfer(self, toAcc, axieId, gas, gasPrice, nonce):
        transaction = self.contract.functions.safeTransferFrom(
            Web3.toChecksumAddress(self.owner),
            Web3.toChecksumAddress(toAcc),
            int(axieId)
        ).buildTransaction({ 'gas': int(gas), 'gasPrice': int(gasPrice), 'nonce': int(nonce) })
        roninutils.signTransaction(transaction, self.pkey)        

    def breedAxies(self, sireId, matronId, gas, gasPrice, nonce):
        transaction = self.contract.functions.breedAxies(
            int(sireId),
            int(matronId)
        ).buildTransaction({ 'gas': int(gas), 'gasPrice': int(gasPrice), 'nonce': int(nonce) })
        roninutils.signTransaction(transaction, self.pkey)        