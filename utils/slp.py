import os
import json
from web3 import Web3
from encrypt import getPKey
script_dir = os.path.dirname(os.path.dirname( __file__ ))
import roninutils

abiPath = rf"{script_dir}/abi/slp.json"
contractAdd = roninutils.SLP_CONTRACT

class SlpContract:
    def __init__(self, owner):
        self.w3 = roninutils.getWeb3Provider()
        self.owner = owner
        self.pkey = getPKey(owner).decode('ascii')
        with open(abiPath, encoding='utf-8') as f:
            abi = json.load(f)
        self.contract = self.w3.eth.contract(
            address=Web3.toChecksumAddress(contractAdd),
            abi=abi
        )
    
    def transfer(self, toAcc, amount, gas, gasPrice, nonce):
        transaction = self.contract.functions.transfer(
            Web3.toChecksumAddress(toAcc),
            int(amount)
        ).buildTransaction({ 'gas': int(gas), 'gasPrice': int(gasPrice), 'nonce': int(nonce) })
        roninutils.signTransaction(transaction, self.pkey)        

    def claim(self, amount, timestamp, signature, gas, gasPrice, nonce):
        transaction = self.contract.functions.checkpoint(
            Web3.toChecksumAddress(self.account),
            int(amount),
            int(timestamp),
            signature
        ).buildTransaction({ 'gas': int(gas), 'gasPrice': int(gasPrice), 'nonce': int(nonce) })
        roninutils.signTransaction(transaction, self.pkey)        