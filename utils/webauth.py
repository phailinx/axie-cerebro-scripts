import os
from web3 import Web3
from encrypt import getPKey
script_dir = os.path.dirname(os.path.dirname( __file__ ))
from eth_account.messages import encode_defunct

class WebAuth:
    def __init__(self, owner):        
        self.owner = owner
        self.pkey = getPKey(owner).decode('ascii')    

    def signMessage(self, message):
        message = message.replace("##", " ").replace("__", "\n")
        signed_msg = Web3().eth.account.sign_message(encode_defunct(text=message),private_key=self.pkey)
        signature = signed_msg['signature'].hex()                                                     
        print(f'data:{signature}')