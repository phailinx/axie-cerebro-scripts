from cryptography.fernet import Fernet
from openpyxl import load_workbook

# This encrypts the excel file that is specified
excel_path = r""
key_file = r""

def generateKey():
    key= Fernet.generate_key()
    with open(key_file,"wb") as f:
        f.write(key)

def getKey():
    key= open(key_file,"rb").read()
    return key

def getFernet():
    with open(key_file) as f:
        lines = f.readlines()
        if (len(lines) == 0 or lines[0].strip() == ""):
            generateKey()

        key = getKey()
        
    f = Fernet(key)
    return f

def encryptExcel():    
    f = getFernet()
    book = load_workbook(excel_path)
    ws = book['Sheet1']
    rowIndex = 1
    for row in ws.values:
        if (row[0] == "Name" or row[3] == "1" or row[0] == None):
            rowIndex = rowIndex + 1
            continue

        ws.cell(column=1, row=rowIndex, value=row[0])
        ws.cell(column=2, row=rowIndex, value=row[1].replace('ronin:', '0x').lower())
        pkey = f.encrypt(bytes(row[2], 'utf-8'))
        ws.cell(column=3, row=rowIndex, value=pkey)
        ws.cell(column=4, row=rowIndex, value=1)
        rowIndex = rowIndex + 1

    book.save(excel_path)
    print("Excel Encrypted")

def getPKey(ronin):
    f = getFernet()
    book = load_workbook(excel_path)
    ws = book['Sheet1']
    for row in ws.values:
        if (row[0] == "Name"):
            continue
        if (ronin == row[1]):
            return f.decrypt(bytes(row[2], 'utf-8'))

    raise Exception("Ronin not found in excel")

if __name__ == '__main__':
    try:
        encryptExcel()
    except Exception as e: 
        print(e)