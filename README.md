# Axie Cerebro Scripts
Companion scripts for Axie Cerebro. This integration is for users who prefer storing their private keys outside of Cerebro.

## Security
A standard encryption template is provided for storing keys, but users are free to deploy their own private key access logic, so long as the entry points in the `main.py` file preserved and the underlying functions that return the signed transactions as expected by Cerebro.

## Software Installs
1) Visual Studio Code (install pylance extension that will be recommend by VS Code)
2) Python (download latest make sure to choose custom install that install for everybody on the computer)
3) Install microsoft C++ build tools: https://visualstudio.microsoft.com/visual-cpp-build-tools/
4) open command prompt -> cd to script folder -> `run pip install -r requirements.txt`

## Post install setup
1) Create an empty text file and call it `encrypt.key` and place it anywhere you prefer **(remember the location)**
2) Determine which method of encryption you prefer (private key or seed phrase)
3) Create a spreadsheet with the following recommended fields based on which private key setup you prefer. **(remember the location)**
    
    For Private Key:
    
    Name	Ronin	Pkey	IsEncrypt

    For Seed Phrase:
    
    Name	Seed	Size	IsEncrypt

4) Rename the appropriate `encrypt-XXX.py` file to `encrypt.py` based on the private key setup you prefer.
5) Open the `encrypt.py` file and edit the `excel_path` and `key_path` fields (located near the top of the file) based on the where you placed the files in step 1) and 2)

## Editing the Excel file
This file will need to be encrypted for security reasons. You can do so by running `encrypt-excel.bat` that encrypts the excel file where the rows have `IsEncrypt` set to `0`.

### Field explanations:
**a) For Private Key Setup**

Name - A name for you to identify what account this row is for

Ronin - The ronin address of the account

PKey - The private key that you will paste here from the Ronin Wallet

IsEncrypt - Set to 0 to indicate this row needs to be encrypted. 

**b) For Seed Phrase Setup**

Name - A name for you to identify the Seed Phrase

Seed - The mnemonic/12 word seed phrase for the wallet

Size - The number of accounts that belong to this seed phrase. In other words, how many accounts have been generated under this seed phrase.

IsEncrypt - Set to 0 to indicate this row needs to be encrypted. 