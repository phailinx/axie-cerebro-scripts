#version 1.0

import sys
from utils.rontoken import RonToken
from utils.slp import SlpContract
from utils.axs import AxsContract
from utils.weth import WethContract
from utils.axie import AxieContract
from utils.market import MktContract
from utils.webauth import WebAuth
from utils.scatter import ScatterContract

if __name__ == '__main__' and len(sys.argv) >= 1:
    args = sys.argv    
    try:        
        type = args[1]
        if type == 'slp-transfer':
            owner = args[2]
            toRonin = args[3]
            amount = args[4]
            gas = args[5]
            gasPrice = args[6]
            nonce = args[7]   
            contract = SlpContract(owner)
            contract.transfer(toRonin,amount,gas,gasPrice,nonce)
        elif type == 'slp-claim':
            owner = args[2]
            amount = args[3]
            timestamp = args[4]
            signature = args[5]
            gas = args[6]
            gasPrice = args[7]
            nonce = args[8]   
            contract = SlpContract(owner)
            contract.claim(amount, timestamp, signature, gas, gasPrice, nonce)
        elif type == "axs-transfer":
            owner = args[2]
            toRonin = args[3]
            amount = args[4]
            gas = args[5]
            gasPrice = args[6]
            nonce = args[7]   
            contract = AxsContract(owner)
            contract.transfer(toRonin,amount, gas, gasPrice, nonce)
        elif type == "weth-transfer":
            owner = args[2]
            toRonin = args[3]
            amount = args[4]
            gas = args[5]
            gasPrice = args[6]
            nonce = args[7]    
            contract = WethContract(owner)
        elif type == "ron-transfer":
            owner = args[2]
            toRonin = args[3]
            amount = args[4]
            gas = args[5]
            gasPrice = args[6]
            nonce = args[7]    
            contract = RonToken(owner)
            contract.transfer(toRonin, amount, gas, gasPrice, nonce)
        elif type == "axie-transfer":
            owner = args[2]
            toRonin = args[3]
            axieId = args[4]
            gas = args[5]
            gasPrice = args[6]
            nonce = args[7]   
            contract = AxieContract(owner)
            contract.transfer(toRonin,axieId, gas, gasPrice, nonce)
        elif type == "axie-breed":
            owner = args[2]
            sireId = args[3]
            matronId = args[4]
            gas = args[5]
            gasPrice = args[6]
            nonce = args[7]      
            contract = AxieContract(owner)
            contract.breedAxies(sireId, matronId, gas, gasPrice, nonce)
        elif type == "sign-message":            
            owner = args[2]
            message = args[3]
            contract = WebAuth(owner)
            contract.signMessage(message)
        elif type == "cancel-axie-sale":            
            owner = args[2]
            listingIndex = args[3]
            gas = args[4]
            gasPrice = args[5]
            nonce = args[6]
            contract = MktContract(owner)
            contract.cancelAuction(listingIndex, gas, gasPrice, nonce)
        elif type == "create-axie-sale":            
            owner = args[2]
            axieId = args[3]
            startingPrice = args[4]
            endPrice = args[5]
            duration = args[6]
            gas = args[7]
            gasPrice = args[8]
            nonce = args[9]            
            contract = MktContract(owner)
            contract.createAuction(axieId, startingPrice, endPrice, duration, gas, gasPrice, nonce)
        elif type == "scatter-approve":
            owner = args[2]
            token = args[3]            
            gas = args[4]
            gasPrice = args[5]
            nonce = args[6]            
            contract = ScatterContract(owner)
            contract.approveToken(token, gas, gasPrice, nonce)
        elif type == "scatter-tokens":
            owner = args[2]
            token = args[3]            
            recipients = args[4]
            values = args[5]
            gas = args[6]
            gasPrice = args[7]
            nonce = args[8]            
            contract = ScatterContract(owner)
            contract.scatterTokens(token, recipients, values, gas, gasPrice, nonce)
    except Exception as err:
        print(err)